/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Лера
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
 
/**
 * Simple Java program to connect to MySQL database running on localhost and
 * running SELECT and INSERT query to retrieve and add data.
 * @author Javin Paul
 */
public class JavaToMySQL {
 
    // JDBC URL, username and password of MySQL server
    private static final String url = "jdbc:mysql://localhost:3306/FlamesPC";
    private static final String user = "root";
    private static final String password = "";
 
    // JDBC variables for opening and managing connection
    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;
    
    public String getString(int pageId)
    	{String query = "select page_text from pages where id = " + pageId;
        String pageText = null;
 
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);
            // getting Statement object to execute query
            stmt = con.createStatement();
            // executing SELECT query
            rs = stmt.executeQuery(query);
            while (rs.next())
            	{pageText = rs.getString(1);
            	}
        	}
        catch (SQLException sqlEx)
        	{sqlEx.printStackTrace();
        	}
        finally
        	{//close connection ,stmt and resultset here*/
        	 try { con.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        	}
        return pageText;
    	}
    
    public pageLink getLink(int pageId, int win)
	{String query = "select * from links where page_id = " + pageId + " and win = " + win;
         pageLink Link = new pageLink(pageId);

    try {
        // opening database connection to MySQL server
        con = DriverManager.getConnection(url, user, password);
        // getting Statement object to execute query
        stmt = con.createStatement();
        // executing SELECT query
        rs = stmt.executeQuery(query);
        while (rs.next())
        	{Link.setEverything(rs.getInt(2), rs.getString(3), win);// = rs.getString(1);
        	}
    	}
    catch (SQLException sqlEx)
    	{sqlEx.printStackTrace();
    	}
    finally
    	{//close connection ,stmt and resultset here
    	 try { con.close(); } catch(SQLException se) { /*can't do anything */ }
    	 try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
    	 try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
    	}
    return Link;
	}
    
    public int getStatus(int pageId)
    	{String query = "select page_status from pages where id = " + pageId;
         int status = 0;
 
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);
            // getting Statement object to execute query
            stmt = con.createStatement();
            // executing SELECT query
            rs = stmt.executeQuery(query);
            while (rs.next())
            	{status = rs.getInt(1);
            	}
        	}
        catch (SQLException sqlEx)
        	{sqlEx.printStackTrace();
        	}
        finally
        	{//close connection ,stmt and resultset here*/
        	 try { con.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        	}
        return status;
    	}
    
    public int getExtra(int pageId)
    	{String query = "select extra from pages where id = " + pageId;
         int status = 0;
 
        try {
            // opening database connection to MySQL server
            con = DriverManager.getConnection(url, user, password);
            // getting Statement object to execute query
            stmt = con.createStatement();
            // executing SELECT query
            rs = stmt.executeQuery(query);
            while (rs.next())
            	{status = rs.getInt(1);
            	}
        	}
        catch (SQLException sqlEx)
        	{sqlEx.printStackTrace();
        	}
        finally
        	{//close connection ,stmt and resultset here*/
        	 try { con.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { stmt.close(); } catch(SQLException se) { /*can't do anything */ }
        	 try { rs.close(); } catch(SQLException se) { /*can't do anything */ }
        	}
        return status;
    	}
    
}