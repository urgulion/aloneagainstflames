/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.util.Random;

/**
 *
 * @author Лера
 */
public class CharacterList {
    public String[] points = {"Здоровье", "Разум", "Удача", "Магия"};
    public String[] chara = {"Сила", "Выносливость", "Сила воли", "Ловкость",
                               "Привлекательность", "Габариты", "Интеллект", "Образование"};
    public String[] skills = {"Археология", "Библиотечный навык", "Быстрый разговор", "Взлом",
                                "Вождение", "Выслеживание", "Дикая природа", "Драки",
                                "Запугивание", "Лазание", "Маскировка", "Наездник",
                                "Наука (Ботаника)", "Первая помощь", "Поиск скрытого", "Психология",
                                "Скрытность", "Слух", "Уклонение", "Шарм"};
    
    /*public int[] numbPoints = {0,0,0,0};
    public int[] numbChara = {0,0,0,0,0,0,0,0};
    */
    
    public int[] numbPoints = {20,20,20,20};
    public int[] maxPoints = {20,20,20,20};
    public int[] numbChara = {50,50,60,60,50,70,80,40};
    public int[] numbSkills = {5,20,5,5,20,10,10,25,15,20,5,5,5,30,25,10,20,20,30,15};
    
    public int profession;
    public int penalty = 0;
    
    public void setList(int[] chara, int[] items)
        {for (int i=0; i<8; i++)
            {numbChara[items[i]]=chara[i];
            }
        final Random random = new Random(); 
        numbPoints[0] = (chara[items[2]] + chara[items[6]]) / 10;
        numbPoints[1] = chara[items[3]];
        numbPoints[2] = random.nextInt((18)+3);
        numbPoints[3] =  chara[items[3]] / 5;
        for (int i=0; i<4; i++)
            {maxPoints[i]=numbPoints[i];
            }
        }
    
    public String printNoSkills()
        {String tmp = "<html><p>ОЧКИ</p>";
         for (int i=0; i<4; i++)
            {tmp = tmp + "<p>" + points[i] + " " + numbPoints[i] + "</p>";
            }
         tmp = tmp + "<br><p>ХАРАКТЕРИСТИКИ</p>";
         for (int i=0; i<8; i++)
            {tmp = tmp + "<p>" + chara[i] + " " + numbChara[i] + "</p>";
            }
         tmp = tmp + "</html>";
         return tmp;
        }
    
    public String printWithSkills()
        {String tmp = "<html><p>ОЧКИ</p>";
         for (int i=0; i<4; i++)
            {tmp = tmp + "<p>" + points[i] + " " + numbPoints[i] + "</p>";
            }
         tmp = tmp + "<br><p>ХАРАКТЕРИСТИКИ</p>";
         for (int i=0; i<8; i++)
            {tmp = tmp + "<p>" + chara[i] + " " + numbChara[i] + "</p>";
            }
         tmp = tmp + "<br><p>НАВЫКИ</p>";
         for (int i=0; i<20; i++)
            {tmp = tmp + "<p>" + skills[i] + " " + numbSkills[i] + "</p>";
            }
         tmp = tmp + "</html>";
         return tmp;
        }
    
    public void setSkills(int prof)
        {profession=prof;
         switch(prof)
            {case 1:
                {numbSkills[14]=70;
                 numbSkills[1]=60;
                 numbSkills[2]=50;
                 numbSkills[19]=50;
                 break;
                }
             case 2:
                {numbSkills[13]=70;
                 numbSkills[12]=60;
                 numbSkills[14]=50;
                 numbSkills[15]=50;
                 break;
                }
             case 3:
                {numbSkills[19]=70;
                 numbSkills[14]=60;
                 numbSkills[2]=50;
                 numbSkills[3]=50;
                 break;
                }
             case 4:
                {numbSkills[14]=70;
                 numbSkills[1]=60;
                 numbSkills[19]=50;
                 numbSkills[8]=50;
                 break;
                }
             case 5:
                {numbSkills[12]=70;
                 numbSkills[1]=60;
                 numbSkills[14]=50;
                 numbSkills[15]=50;
                 break;
                }
             default: break;
            }
        }
    
    public void setHobby(String[] items)
        {for (int i=0; i<8; i++)
            {for (int j=0; j<20; j++)
                {if (skills[j]==items[i])
                    {numbSkills[i]+=20;
                    }
                }
            }
        }
    
    public int getAnything (int status, int id)
        {switch(status)
            {case 1:
                {return numbPoints[id];
                }
             case 2:
                {return numbChara[id];
                }
             case 3:
                {return numbSkills[id];
                }
             default: break;
            }
        return 0;
        }
    
    public String getName (int status, int id)
        {switch(status)
            {case 1:
                {return points[id];
                }
             case 2:
                {return chara[id];
                }
             case 3:
                {return skills[id];
                }
             default: break;
            }
        return "";
        }
    
    public String changePoints(int extra)
        {String changeLog = "";
         if (extra%100>10)
            {if (numbPoints[extra/100-1]<maxPoints[extra/100-1])
                {final Random random = new Random();
                 int tmp = random.nextInt((extra%10)+1);
                 numbPoints[extra/100-1]+=tmp;
                 changeLog = "Вы восстановили " + tmp + "единиц ";
                 switch (extra/100)
                    {case 1:
                        {changeLog+="Здоровья.";
                        break;
                        }
                     case 2:
                        {changeLog+="Разума";
                        break;
                        }
                     case 3:
                        {changeLog+="Удачи";
                        break;
                        }
                     case 4:
                        {changeLog+="Магии";
                        break;
                        }
                     default: break;
                    }
                }
            }
         else
            {final Random random = new Random();
             int tmp = random.nextInt((extra%10)+1);
             numbPoints[extra/100-1]-=tmp;
             if (numbPoints[extra/100]>=0)
                {changeLog = "Вы потеряли " + tmp + " единиц ";
                 switch (extra/100)
                    {case 1:
                        {changeLog+="Здоровья.";
                        break;
                        }
                     case 2:
                        {changeLog+="Разума";
                        break;
                        }
                     case 3:
                        {changeLog+="Удачи";
                        break;
                        }
                     case 4:
                        {changeLog+="Магии";
                        break;
                        }
                     default: break;
                    }
                }
             else 
                {numbPoints[extra/100-1]=0;
                changeLog = "Вы потеряли единицы ";
                 switch (extra/100)
                    {case 1:
                        {changeLog+="Здоровья.";
                        break;
                        }
                     case 2:
                        {changeLog+="Разума";
                        break;
                        }
                     case 3:
                        {changeLog+="Удачи";
                        break;
                        }
                     case 4:
                        {changeLog+="Магии";
                        break;
                        }
                     default: break;
                    }
                }
            }
         return changeLog;
        }
    
    public void lvlUP (int numb)
        {numbSkills[numb]+=1;
        }
    
    public boolean dead()
        {if (numbPoints[0]==0)
            return true;
        else return false;
        }
}
