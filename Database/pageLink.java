/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Лера
 */
public class pageLink {
	private int pageId;
	private int linkId;
	private String textLink;
	private int win;
	
	public pageLink(int id)
		{pageId = id;		
		}
	
	public void setLink(int id)
		{pageId = id;
		}
	
	public void setEverything(int id, String linkText, int Win)
		{linkId = id;
		 textLink = linkText;
		 win = Win;
		}
	
	
	public int getPageId()
		{return pageId;
		}
	
	public int getLinkId()
		{return linkId;
		}
	
	public String getText()
		{return textLink;
		}
	
	public int getWin()
		{return win;
		}
	
	public void printstuff ()
		{
		 System.out.print("page id = " + pageId + "\n");
		 System.out.print("link id = " + linkId + "\n");
		 System.out.print("text = " + textLink + "\n");
		 System.out.print("win = " + win + "\n");
		}
	
}
