/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;
import Controller.Gamelogic;
import Database.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 *
 * @author Лера
 */
public class PagePanel extends javax.swing.JPanel {
    GUI gui;
    private Gamelogic logic = new Gamelogic();
    pageLink link = new pageLink(1);
    JavaToMySQL database = new JavaToMySQL();
    CharacterList list;
    Secrets secret;
    int id;
    /**
     * Creates new form PageFrame
     */
    public PagePanel() {
        initComponents();
        }
    
    public void showPage(JavaToMySQL database, GUI gui, CharacterList list, Gamelogic logic, Secrets secret)
        {//jButton1.removeAll();
         this.gui = gui;
         this.list = list;
         this.logic = logic;
         this.database = database;
         id = gui.getID();
         jLabel1.setText("<html>" + database.getString(id) + "</html>");
         
         int[] noSkill = {8, 23, 38, 233, 134, 261, 59, 71, 128, 102, 226, 239, 249, 265};
         
         if (id==1)
            {//не содержит лист персонажа
             //содержит одну кнопку
             jButton1.setVisible(true);
             jButton2.setVisible(false);
             jButton3.setVisible(false);
             jButton4.setVisible(false);
             jButton5.setVisible(false);
             jButton6.setVisible(false);
             jButton7.setVisible(false);
             jButton8.setVisible(false);
             jLabel2.setVisible(false);
             jLabel3.setVisible(false);
             
             jButton1.setText("--> Продолжить");
             
             link.setLink(id);
             link = database.getLink(id, 0);
             
             ButtonListener listener = new ButtonListener();
             jButton1.addActionListener(listener);
            }
         else
            {boolean tmp=false;
             for (int i=0; i<14; i++)
                {if (id==noSkill[i])
                    tmp=true;
                }
             if (tmp)
                {//страницы, на которых не отображаются навыки
                 jLabel2.setText(list.printNoSkills());
                 jLabel2.setVisible(true);
                }
             else {//отображается весь лист персонажа
                 jLabel2.setText(list.printWithSkills());
                 jLabel2.setVisible(true);
                 }
             
             int pageStatus = database.getStatus(id);
             //проверка статуса страницы
             if (pageStatus==0) //обычная страница
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                if ((id==26)||(id==52)) //получение штрафа
                    {list.penalty=1;
                    }
                
                 link.setLink(id);
                 if (id==8)
                    {if ((list.getAnything(2, 5))==40)
                        link = database.getLink(id, 0);
                     else
                        link = database.getLink(id, 1);
                    }
                 else
                 link = database.getLink(id, 0);
                jButton1.setText(link.getText());
                 ButtonListener listener = new ButtonListener();
                 jButton1.addActionListener(listener);
                }
             if ((pageStatus>0)&&(pageStatus<4)) //проверка навыка/характеристики
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                int extra = database.getExtra(id);
                String name = list.getName(extra/100, extra%100);
                if (pageStatus==1)
                    jButton1.setText("Проверка " + name);
                if (pageStatus==2)
                    jButton1.setText("Тяжелая проверка " + name);
                if (pageStatus==3)
                    jButton1.setText("Экстремальная проверка " + name);
                
                RollDices roll = new RollDices();
                jButton1.addActionListener(roll);      
                }
             if (pageStatus==5) //изменение кол-ва очков
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(true);
                
                if (id==141)
                    {secret.check[0]=true;
                    }
                
                if ((id==258)||(id==217))
                    {String change = list.changePoints(102);
                    change+=list.changePoints(203);
                    jLabel3.setText(change);
                    }
                else
                    {int extra = database.getExtra(id);
                    jLabel3.setText(list.changePoints(extra));
                    }
                link = database.getLink(id, 0);
                jButton1.setText(link.getText());
                ButtonListener listener = new ButtonListener();
                jButton1.addActionListener(listener);
                }
             if (pageStatus==6) //проверка навыка на выбор
                {jButton1.setVisible(true);
                jButton2.setVisible(true);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                jButton1.setText("Проверка Вождение");
                jButton2.setText("Тяжелая проверка Психология");
                
                jButton1.addActionListener(new ActionListener()
                        {@Override
                        public void actionPerformed(ActionEvent e)
                            {if (id!=144)
                                {link = database.getLink(id, 0);
                                 gui.page(link.getLinkId());
                                }
                            final Random random = new Random();
                            int roll = random.nextInt((100)+1);
                            
                            boolean check = logic.Check(roll, 1, 3, 4, list);
                            link.setLink(id);
                            if (check)
                              {jLabel3.setVisible(true);
                               jLabel3.setText("Вы получили число " + roll + ". Вы прошли проверку. Уровень навыка повысился на единицу.");
                               list.lvlUP(4);
                               link = database.getLink(id, 1);
                               }
                            else
                               {jLabel3.setVisible(true);
                                jLabel3.setText("Вы получили число " + roll + ". Вы провалили проверку.");
                                link = database.getLink(id, 0);
                               }
                            jButton1.setText(link.getText());
                            ButtonListener listener = new ButtonListener();
                            jButton1.removeActionListener(this);
                            jButton1.addActionListener(listener);
                            jButton2.removeAll();
                            }
                        }
                    );  
                
                jButton2.addActionListener(new ActionListener()
                        {@Override
                        public void actionPerformed(ActionEvent e)
                            {if (id!=144)
                                {link = database.getLink(id, 0);
                                 gui.page(link.getLinkId());
                                }
                            final Random random = new Random();
                            int roll = random.nextInt((100)+1);
                            
                            boolean check = logic.Check(roll, 2, 3, 15, list);
                            link.setLink(id);
                            if (check)
                              {jLabel3.setVisible(true);
                               jLabel3.setText("Вы получили число " + roll + ". Вы прошли проверку. Уровень навыка повысился на единицу.");
                               list.lvlUP(15);
                               link = database.getLink(id, 1);
                               }
                            else
                               {jLabel3.setVisible(true);
                                jLabel3.setText("Вы получили число " + roll + ". Вы провалили проверку.");
                                link = database.getLink(id, 0);
                               }
                            jButton2.setText(link.getText());
                            ButtonListener listener = new ButtonListener();
                            jButton2.removeActionListener(this);
                            jButton2.addActionListener(listener);
                            jButton1.removeAll();
                            }
                        }
                     );
                }
             if (pageStatus==7) //выбор профессии
                {jButton1.setVisible(true);
                jButton2.setVisible(true);
                jButton3.setVisible(true);
                jButton4.setVisible(true);
                jButton5.setVisible(true);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                link = database.getLink(id, 0);
                jButton1.setText(link.getText());
                
                jButton1.addActionListener(new ActionListener()
                    {@Override
                     public void actionPerformed(ActionEvent e)
                        {jButton1.removeActionListener(this);
                         int pageStatus = database.getStatus(id);
                        switch (pageStatus) {
                            case 7:
                                gui.page(102);
                                break;
                            case 8:
                                gui.hobbyPage();
                                break;
                            default:
                                link = database.getLink(id, 0);
                                gui.page(link.getLinkId());
                                break;
                        }
                        }
                    }
                );
                
                link = database.getLink(id, 1);
                jButton2.setText(link.getText());
                jButton2.addActionListener(new ActionListener()
                    {@Override
                     public void actionPerformed(ActionEvent e)
                        {jButton1.removeAll();
                         jButton2.removeActionListener(this);
                         
                         int pageStatus = database.getStatus(id);
                        switch (pageStatus) {
                            case 7:
                                gui.page(226);
                                break;
                            case 8:
                                gui.hobbyPage();
                                break;
                            default:
                                link = database.getLink(id, 0);
                                gui.page(link.getLinkId());
                                break;
                        }
                        }
                    }
                );
                
                link = database.getLink(id, 2);
                jButton3.setText(link.getText());
                jButton3.addActionListener(new ActionListener()
                    {@Override
                     public void actionPerformed(ActionEvent e)
                        {jButton1.removeAll();
                         jButton3.removeActionListener(this);
                         
                         int pageStatus = database.getStatus(id);
                        switch (pageStatus) {
                            case 7:
                                gui.page(239);
                                break;
                            case 8:
                                gui.hobbyPage();
                                break;
                            default:
                                link = database.getLink(id, 0);
                                gui.page(link.getLinkId());
                                break;
                        }
                        }
                    }
                );
                
                link = database.getLink(id, 3);
                jButton4.setText(link.getText());
                jButton4.addActionListener(new ActionListener()
                    {@Override
                     public void actionPerformed(ActionEvent e)
                        {jButton4.removeActionListener(this);
                         
                         int pageStatus = database.getStatus(id);
                        switch (pageStatus) {
                            case 7:
                                gui.page(249);
                                break;
                            case 8:
                                gui.hobbyPage();
                                break;
                            default:
                                link = database.getLink(id, 0);
                                gui.page(link.getLinkId());
                                break;
                        }
                        }
                    }
                );
                
                link = database.getLink(id, 4);
                jButton5.setText(link.getText());
                jButton5.addActionListener(new ActionListener()
                    {@Override
                     public void actionPerformed(ActionEvent e)
                        {jButton5.removeActionListener(this);
                         
                         int pageStatus = database.getStatus(id);
                        switch (pageStatus) {
                            case 7:
                                gui.page(265);
                                break;
                            case 8:
                                gui.hobbyPage();
                                break;
                            default:
                                link = database.getLink(id, 0);
                                gui.page(link.getLinkId());
                                break;
                        }
                        }
                    }
                );
                
                }
             if (pageStatus==8) //назначение профессиональных навыков
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                 switch(id)
                    {case 102:
                        {list.setSkills(1);
                         break;
                        }
                     case 226:
                        {list.setSkills(2);
                         break;
                        }
                     case 239:
                        {list.setSkills(3);
                         break;
                        }
                     case 249:
                        {list.setSkills(4);
                         break;
                        }
                     case 265:
                        {list.setSkills(5);
                         break;
                        }
                     default: break;
                    }
                 link = database.getLink(id, 0);
                 jButton1.setText(link.getText());
                 ButtonListener listener = new ButtonListener();
                 jButton1.addActionListener(listener);
                }
             
             // ==4 драка
             
            if (pageStatus==10)// специальные страницы и секреты
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                 link.setLink(id);
                 link = database.getLink(id, 0);
                 jButton1.setText(link.getText());
                 ButtonListener listener = new ButtonListener();
                 jButton1.addActionListener(listener);
                 
                 if (id==166)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Исследовать результаты вчерашней прогулки.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(178);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                  if (id==7)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Воспользоваться увиденной ранее дорогой.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(107);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                  if (id==180)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Отправиться на встречу с Арбогастом.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(169);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                  if (id==64)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Посмотреть при свете дня, что же произошло вчера вечером.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(70);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                  if (id==40)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Заклинание Арбогаста.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(50);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                  if (id==209)
                    {if (secret.check(database.getExtra(id)))
                        {jButton2.setText("Скомандовать звездам сжечь всех жителей деревни.");
                        jButton2.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton2.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(231);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        jButton3.setText("Скомандовать звездам освободить вас.");
                        jButton3.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton3.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(243);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        jButton4.setText("Скомандовать звездам уйти.");
                        jButton4.addActionListener(new ActionListener()
                            {@Override
                             public void actionPerformed(ActionEvent e)
                                {jButton4.removeActionListener(this);

                                int pageStatus = database.getStatus(id);
                                switch (pageStatus) {
                                    case 10:
                                        gui.page(255);
                                        break;
                                    default:
                                        link = database.getLink(id, 0);
                                        gui.page(link.getLinkId());
                                        break;
                                }
                                }
                            }
                        );
                        }
                    }
                if (id==198) //использование заклинания (проверка маны)
                    {jLabel3.setVisible(true);
                    jLabel3.setText("<html><p>Проверка Магии. Каждое очко магии дает вам 10% на успех.\n" +
                                        "Например, если у вас 6 очков магии, вы делаете проверку на 60\n" +
                                        "(т. е. вы должны выкинуть число меньшее или равное 60).</p></html>");
                    
                    jButton1.setText("Проверка магии");
                    
                    RollMagic rollm = new RollMagic();
                    jButton1.addActionListener(rollm);  
                    }
                if (id==99)
                    {final Random random = new Random();
                     int roll = random.nextInt((100)+1);
                     if (roll<=50)
                        {link = database.getLink(id, 1);
                        }
                    }
                 if (id==29)
                    {if (list.getAnything(2, 5)<=list.getAnything(2, 3))
                        {link = database.getLink(id, 1);
                        }
                    }
                 if (id==55)
                    {jLabel3.setVisible(true);
                     jLabel3.setText(list.changePoints(112));
                     if (list.numbPoints[0]<(list.maxPoints[0]/2))
                         {link = database.getLink(id, 1);
                         }
                    }
                }
            if (pageStatus==13)// секрет найден
                {jButton1.setVisible(true);
                jButton2.setVisible(false);
                jButton3.setVisible(false);
                jButton4.setVisible(false);
                jButton5.setVisible(false);
                jButton6.setVisible(false);
                jButton7.setVisible(false);
                jButton8.setVisible(false);
                jLabel3.setVisible(false);
                
                 secret.set(database.getExtra(id));
                               
                 link.setLink(id);
                 link = database.getLink(id, 0);
                 jButton1.setText(link.getText());
                 ButtonListener listener = new ButtonListener();
                 jButton1.addActionListener(listener);
                }
            // ==12 //плохой конец
            // ==11 //хороший конец
            }
        }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setAutoscrolls(true);

        jLabel1.setText("jLabel1");
        jLabel1.setAutoscrolls(true);

        jButton1.setText("jButton1");

        jButton2.setText("jButton2");

        jButton3.setText("jButton3");

        jButton4.setText("jButton4");

        jButton5.setText("jButton5");

        jButton6.setText("jButton6");

        jButton7.setText("jButton7");

        jButton8.setText("jButton8");

        jLabel2.setText("jLabel2");
        jLabel2.setAutoscrolls(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton8, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(37, 37, 37))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 474, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)))
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 757, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 409, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton3)
                            .addComponent(jButton7))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton4)
                            .addComponent(jButton8)))
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );
    }// </editor-fold>//GEN-END:initComponents

    
    public class ButtonListener implements ActionListener  {
        @Override
        public void actionPerformed(ActionEvent e)
            {switch (link.getLinkId()) {
                case 263:
                    jButton1.removeActionListener(this);
                    gui.charaPage();
                    break;
                case 128:
                    jButton1.removeActionListener(this);
                    gui.hobbyPage();
                    break;
                default:
                    jButton1.removeActionListener(this);
                    gui.page(link.getLinkId());
                    break;
            }
            }
    }
    
    public class RollDices implements ActionListener  {
        @Override
        public void actionPerformed(ActionEvent e)
            {final Random random = new Random();
             int roll = random.nextInt((100)+1);
             int extra = database.getExtra(id);
             int pageStatus = database.getStatus(id);
             
             if (list.penalty==1)
                {int roll2 = random.nextInt((100)+1);
                 int roll3;
                 if (roll>roll2)
                     roll3=roll;
                 else
                     roll3=roll2;
                 boolean check = logic.Check(roll3, pageStatus, extra/100, extra%100-1, list);
                 link.setLink(id);
                 if (check)
                    {jLabel3.setVisible(true);
                    if(extra/100==3)
                        {jLabel3.setText("Вы получили числа " + roll + " и " + roll2 + ". Вы прошли проверку. Уровень навыка повысился на единицу.");
                        list.lvlUP(extra%100-1);
                        }
                    else
                        jLabel3.setText("Вы получили числа " + roll + " и " + roll2 + ". Вы прошли проверку");
                    link = database.getLink(id, 1);
                    }
                 else
                    {jLabel3.setVisible(true);
                     jLabel3.setText("Вы получили числа " + roll + " и " + roll2 + ". Вы провалили проверку.");
                     link = database.getLink(id, 0);
                    }
                 jButton1.setText(link.getText());
                 ButtonListener listener = new ButtonListener();
                 jButton1.removeActionListener(this);
                 jButton1.addActionListener(listener);
                }
             else
                {boolean check = logic.Check(roll, pageStatus, extra/100, extra%100-1, list);
                link.setLink(id);
                if (check)
                  {jLabel3.setVisible(true);
                   if(extra/100==3)
                        {jLabel3.setText("Вы получили число " + roll + ". Вы прошли проверку. Уровень навыка повысился на единицу.");
                         list.lvlUP(extra%100-1);
                        }
                   link = database.getLink(id, 1);
                   }
                else
                   {jLabel3.setVisible(true);
                    jLabel3.setText("Вы получили число " + roll + ". Вы провалили проверку.");
                    link = database.getLink(id, 0);
                   }
                jButton1.setText(link.getText());
                ButtonListener listener = new ButtonListener();
                jButton1.removeActionListener(this);
                jButton1.addActionListener(listener);
                }
            }
    }
    
    public class RollMagic implements ActionListener  {
        @Override
        public void actionPerformed(ActionEvent e)
            {final Random random = new Random();
             int roll = random.nextInt((100)+1);
             int magic = list.getAnything(1, 3);
             if (roll>magic)
                {jLabel3.setVisible(true);
                 jLabel3.setText("Вы получили число " + roll + ". Вы провалили проверку.");
                 link = database.getLink(id, 0);
                }
             else
                {jLabel3.setVisible(true);
                 jLabel3.setText("Вы получили число " + roll + ". Вы прошли проверку.");
                 link = database.getLink(id, 1);
                }
            }
    }
    
    //panel1 - панель для кнопок
    //panel2 - панель для листа персонажа
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
