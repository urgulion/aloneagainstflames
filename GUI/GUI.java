package GUI;

import javax.swing.*;
import java.awt.*;
import Database.*;
import Controller.*;

/**
 *
 * @author Лера
 */
public class GUI extends JFrame  {
    private MainPage main;
    private PagePanel pageFrame = new PagePanel();
    private CharaPage pageChara = new CharaPage();
    private HobbyPage hobbyFrame = new HobbyPage();
    
    private JavaToMySQL databaseData = new JavaToMySQL();
    private Gamelogic logic = new Gamelogic();
    private CharacterList list = new CharacterList();
    private Secrets secret = new Secrets();
    
    private test test1 = new test();
    
    private int pageID;
    
    public GUI()
        {super("Alone against the flames");
         setBounds(200, 200, 800, 600);
         main  = new MainPage();
	 mainPage();
	 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
        }
    
    public void mainPage() //стартовая страница
        {main.showMain(databaseData, this);
         add(main);
         add(pageFrame);
         }
    
    public void page(int id) //обычная страница
        {if (id==1)
            remove(main);
        else if (id==8)
            remove(pageChara);
        else if (id==144)
            remove(hobbyFrame);
        else
            remove(pageFrame);
         repaint();
         pageID = id;
         pageFrame.showPage(databaseData, this, list, logic, secret);
         add(pageFrame);
         pageFrame.revalidate();
        }
    
    public void charaPage() //распределение характеристик
        {remove(pageFrame);
         repaint();
         pageID = 263;
         pageChara.showPage(databaseData, this, list);
         add(pageChara);
         pageChara.revalidate();
        }
    
     public void hobbyPage() //распределение навыков
        {remove(pageFrame);
         repaint();
         pageID = 128;
         hobbyFrame.showPage(databaseData, this, list);
         add(hobbyFrame);
         hobbyFrame.revalidate();
        }
    
    public int getID()
        {return pageID;
        }
    
    public void setID(int id)
        {pageID = id;
        }
    
}
